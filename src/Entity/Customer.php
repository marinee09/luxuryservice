<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="Customer")
 * @ORM\Entity
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="namesociety", type="string", length=150, nullable=true)
     */
    private $namesociety;

    /**
     * @var string|null
     *
     * @ORM\Column(name="typesociety", type="string", length=150, nullable=true)
     */
    private $typesociety;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post", type="string", length=150, nullable=true)
     */
    private $post;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=20, nullable=true)
     */
    private $telephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updatedat", type="datetime", nullable=true)
     */
    private $updatedat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNamesociety(): ?string
    {
        return $this->namesociety;
    }

    public function setNamesociety(?string $namesociety): self
    {
        $this->namesociety = $namesociety;

        return $this;
    }

    public function getTypesociety(): ?string
    {
        return $this->typesociety;
    }

    public function setTypesociety(?string $typesociety): self
    {
        $this->typesociety = $typesociety;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPost(): ?string
    {
        return $this->post;
    }

    public function setPost(?string $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(?\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getUpdatedat(): ?\DateTimeInterface
    {
        return $this->updatedat;
    }

    public function setUpdatedat(?\DateTimeInterface $updatedat): self
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    public function __toString()
    {
        return json_encode([
            "id" =>  $this->id,
            "namesociety" => $this->namesociety,
            "typesociety" => $this->typesociety,
            "name" => $this->name,
            "post" => $this->post,
            "telephone" => $this->telephone,
            "email" => $this->email,
            "notes" => $this->notes,
            "createdat" => $this->createdat,
            "updatedat" => $this->updatedat,
        ]);
    }

    //  /**
    //  * Generates the magic method
    //  * 
    //  */
    // public function __toString(){
    //     // to show the name of the Category in the select
    //     return $this->name;
    //     // to show the id of the Category in the select
    //     // return $this->id;
    // }



}
