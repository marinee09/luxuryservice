<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Candidate
 *
 * @ORM\Table(name="Candidate")
 * @ORM\Entity
 * @uniqueEntity(fields="email", message="Email already used")
 */
class Candidate implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gender", type="string", length=0, nullable=true)
     */
    private $gender;

    /**
     * @var string|null
     *
     * @ORM\Column(name="firstname", type="string", length=150, nullable=true)
     */
    private $firstname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lastname", type="string", length=150, nullable=true)
     */
    private $lastname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adress", type="string", length=150, nullable=true)
     */
    private $adress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="string", length=150, nullable=true)
     */
    private $country;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nationality", type="string", length=150, nullable=true)
     */
    private $nationality;

    /**
     * @var string|null
     *
     * @ORM\Column(name="passport", type="string", length=255, nullable=true)
     */
    private $passport;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cv", type="string", length=150, nullable=true)
     */
    private $cv;

    /**
     * @var string|null
     *
     * @ORM\Column(name="picture", type="string", length=150, nullable=true)
     */
    private $picture;

    /**
     * @var string|null
     *
     * @ORM\Column(name="currentlocation", type="string", length=150, nullable=true)
     */
    private $currentlocation;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateofbirth", type="date", nullable=true)
     */
    private $dateofbirth;

    /**
     * @var string|null
     *
     * @ORM\Column(name="placeofbirth", type="string", length=150, nullable=true)
     */
    private $placeofbirth;

    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=150, nullable=true, unique=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="password", type="string", length=150, nullable=true)
     */
    private $password;

    /**
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="availability", type="boolean", nullable=true)
     */
    private $availability;

    /**
     * @var string|null
     *
     * @ORM\Column(name="jobsector", type="string", length=255, nullable=true)
     */
    private $jobsector;

    /**
     * @var string|null
     *
     * @ORM\Column(name="experience", type="string", length=0, nullable=true)
     */
    private $experience;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shortdescription", type="string", length=150, nullable=true)
     */
    private $shortdescription;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="admin", type="boolean", nullable=true)
     */
    private $admin;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updatedat", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedat", type="datetime", nullable=true)
     */
    private $deletedat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Joboffer", inversedBy="candidate")
     * @ORM\JoinTable(name="Candidacy",
     *   joinColumns={
     *     @ORM\JoinColumn(name="Candidate_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="Joboffer_id", referencedColumnName="id")
     *   }
     * )
     */
    private $joboffer;

    /**
     * 
     */

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->joboffer = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getPassport(): ?string
    {
        return $this->passport;
    }

    public function setPassport(?string $passport): self
    {
        $this->passport = $passport;

        return $this;
    }

    public function getCv(): ?string
    {
        return $this->cv;
    }

    public function setCv(?string $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getCurrentlocation(): ?string
    {
        return $this->currentlocation;
    }

    public function setCurrentlocation(?string $currentlocation): self
    {
        $this->currentlocation = $currentlocation;

        return $this;
    }

    public function getDateofbirth(): ?\DateTimeInterface
    {
        return $this->dateofbirth;
    }

    public function setDateofbirth(?\DateTimeInterface $dateofbirth): self
    {
        $this->dateofbirth = $dateofbirth;

        return $this;
    }

    public function getPlaceofbirth(): ?string
    {
        return $this->placeofbirth;
    }

    public function setPlaceofbirth(?string $placeofbirth): self
    {
        $this->placeofbirth = $placeofbirth;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $password)
    {
        $this->plainPassword = $password;

        return $this;
    }

    public function getAvailability(): ?bool
    {
        return $this->availability;
    }

    public function setAvailability(?bool $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getJobsector(): ?string
    {
        return $this->jobsector;
    }

    public function setJobsector(?string $jobsector): self
    {
        $this->jobsector = $jobsector;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(?string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getShortdescription(): ?string
    {
        return $this->shortdescription;
    }

    public function setShortdescription(?string $shortdescription): self
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    public function getAdmin(): ?bool
    {
        return $this->admin;
    }

    public function setAdmin(?bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(?\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getUpdatedat(): ?\DateTimeInterface
    {
        return $this->updatedat;
    }

    public function setUpdatedat(?\DateTimeInterface $updatedat): self
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getDeletedat(): ?\DateTimeInterface
    {
        return $this->deletedat;
    }

    public function setDeletedat(?\DateTimeInterface $deletedat): self
    {
        $this->deletedat = $deletedat;

        return $this;
    }

    /**
     * @return Collection|Joboffer[]
     */
    public function getJoboffer(): Collection
    {
        return $this->joboffer;
    }

    public function addJoboffer(Joboffer $joboffer): self
    {
        if (!$this->joboffer->contains($joboffer)) {
            $this->joboffer[] = $joboffer;
        }

        return $this;
    }

    public function removeJoboffer(Joboffer $joboffer): self
    {
        if ($this->joboffer->contains($joboffer)) {
            $this->joboffer->removeElement($joboffer);
        }

        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return ['ROLE_ADMIN'];
    }

    public function eraseCredentials()
    {

    }

    public function getUsername()
    {
        return $this->email;
    }

    public function __toString()
    {
        return json_encode([
            "id" =>  $this->id,
            "gender" => $this->gender,
            "firstname" => $this->firstname,
            "lastname" => $this->lastname,
            "adress" => $this->adress,
            "country" => $this->country,
            "nationality" => $this->nationality,
            "passport" => $this->passport,
            "cv" => $this->cv,
            "picture" => $this->picture,
            "currentlocation" => $this->currentlocation,
            "dateofbirth" => $this->dateofbirth,
            "placeofbirth" => $this->placeofbirth,
            "email" => $this->email,
            "password" => $this->password,
            "availability" => $this->availability,
            "jobsector" => $this->jobsector,
            "experience" => $this->experience,
            "shortdescription" => $this->shortdescription,
            "admin" => $this->admin,
            "createdat" => $this->createdat,
            "updatedat" => $this->updatedat,
            "notes" => $this->notes,
            "deletedat" => $this->deletedat,
        ]);
    }
    //  /**
    //  * Generates the magic method
    //  * 
    //  */
    // public function __toString(){
    //     // to show the name of the Category in the select
    //     return $this->name;
    //     // to show the id of the Category in the select
    //     // return $this->id;
    // }

    // public function serialize()
    // {
    //     return serialize([
    //         $this->id,
    //         $this->email,
    //         $this->password
    //     ]);
    // }

    // public function unserialize($string)
    // {
    //     list (
    //         $this->id,
    //         $this->email,
    //         $this->password
    //     ) = unserialize($string, ['allowed_classes' => false]);
    // }


}
