<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Joboffer
 *
 * @ORM\Table(name="Joboffer", indexes={@ORM\Index(name="fk_Joboffer_Customer1_idx", columns={"Customer_id"})})
 * @ORM\Entity
 */
class Joboffer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="reference", type="string", length=45, nullable=true)
     */
    private $reference;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="valide", type="boolean", nullable=true)
     */
    private $valide;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="jobtitle", type="string", length=150, nullable=true)
     */
    private $jobtitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="jobtype", type="string", length=150, nullable=true)
     */
    private $jobtype;

    /**
     * @var string|null
     *
     * @ORM\Column(name="location", type="string", length=150, nullable=true)
     */
    private $location;

    /**
     * @var string|null
     *
     * @ORM\Column(name="jobcategory", type="string", length=150, nullable=true)
     */
    private $jobcategory;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="closingdate", type="date", nullable=true)
     */
    private $closingdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="salary", type="string", length=255, nullable=true)
     */
    private $salary;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updatedat", type="datetime", nullable=true)
     */
    private $updatedat;

    /**
     * @var \Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Customer_id", referencedColumnName="id")
     * })
     */
    private $customer_id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Candidate", mappedBy="joboffer")
     */
    private $candidate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->candidate = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getJobtitle(): ?string
    {
        return $this->jobtitle;
    }

    public function setJobtitle(?string $jobtitle): self
    {
        $this->jobtitle = $jobtitle;

        return $this;
    }

    public function getJobtype(): ?string
    {
        return $this->jobtype;
    }

    public function setJobtype(?string $jobtype): self
    {
        $this->jobtype = $jobtype;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getJobcategory(): ?string
    {
        return $this->jobcategory;
    }

    public function setJobcategory(?string $jobcategory): self
    {
        $this->jobcategory = $jobcategory;

        return $this;
    }

    public function getClosingdate(): ?\DateTimeInterface
    {
        return $this->closingdate;
    }

    public function setClosingdate(?\DateTimeInterface $closingdate): self
    {
        $this->closingdate = $closingdate;

        return $this;
    }

    public function getSalary(): ?string
    {
        return $this->salary;
    }

    public function setSalary(?string $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getCreatedat(): ?\DateTimeInterface
    {
        return $this->createdat;
    }

    public function setCreatedat(?\DateTimeInterface $createdat): self
    {
        $this->createdat = $createdat;

        return $this;
    }

    public function getUpdatedat(): ?\DateTimeInterface
    {
        return $this->updatedat;
    }

    public function setUpdatedat(?\DateTimeInterface $updatedat): self
    {
        $this->updatedat = $updatedat;

        return $this;
    }

    public function getCustomerId(): ?Customer
    {
        return $this->customer_id;
    }

    public function setCustomerId(?Customer $customer): self
    {
        $this->customer_id = $customer;

        return $this;
    }

    /**
     * @return Collection|Candidate[]
     */
    public function getCandidate(): Collection
    {
        return $this->candidate;
    }

    public function addCandidate(Candidate $candidate): self
    {
        if (!$this->candidate->contains($candidate)) {
            $this->candidate[] = $candidate;
            $candidate->addJoboffer($this);
        }

        return $this;
    }

    public function removeCandidate(Candidate $candidate): self
    {
        if ($this->candidate->contains($candidate)) {
            $this->candidate->removeElement($candidate);
            $candidate->removeJoboffer($this);
        }

        return $this;
    }

    public function __toString()
    {
        return json_encode([
            "id" =>  $this->id,
            "reference" => $this->reference,
            "description" => $this->description,
            "valide" => $this->valide,
            "notes" => $this->notes,
            "jobtitle" => $this->jobtitle,
            "location" => $this->location,
            "jobcategory" => $this->jobcategory,
            "closingdate" => $this->closingdate,
            "salary" => $this->salary,
            "createdat" => $this->createdat,
            "updatedat" => $this->updatedat,
            "customer_id" => $this->customer_id,

        ]);
        }

    //  /**
    //  * Generates the magic method
    //  * 
    //  */
    // public function __toString(){
    //     // to show the name of the Category in the select
    //     return $this->name;
    //     // to show the id of the Category in the select
    //     // return $this->id;
    // }



}
