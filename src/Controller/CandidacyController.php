<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Candidacy;
use App\Entity\Joboffer;
use Doctrine\Common\Persistence\ObjectManager;

class CandidacyController extends AbstractController
{
    /**
     * @Route("/candidacy/{id}", name="candidacy")
     */
    public function index(Joboffer $joboffer)
    {
        $candidate = $this->getUser();
        $application = $joboffer->addCandidate($candidate);
        
        $em = $this->getDoctrine()->getManager();

        try {
            $em->persist($application);
            $em->flush();

            $this->addFlash(
                'notice',
                'Vous avez bien postulé!'
            );
        }
        catch(\Exception $e) {
            $this->addFlash(
                'error',
                'La candidature n\'a pas pu être enregistrée. Veuillez réessayez.'
            );
        }

        // return $this->redirect('joboffers');
        return $this->render('joboffer/index.html.twig',  compact('joboffer', 'candidaciesByJobId'));
    }
}
