<?php

namespace App\Controller;

use App\Entity\Joboffer;
use App\Form\JobofferType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;

// /**
//  * @Route("/joboffer")
//  */
class JobofferController extends AbstractController
{
    /**
     * @Route("/joboffer", name="joboffer_index", methods="GET")
     */
    public function index(ObjectManager $manager): Response
    {

        $candidate = $this->getUser();

        $query = 'SELECT * FROM Joboffer';

        $manager = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $manager->execute();

        $joboffer = $manager->fetchAll();

        $jobIds = [];
        foreach($joboffer as $job) {
            $jobIds[] = $job["id"];
        }

        $manager = $this->getDoctrine()->getManager()->getConnection()->prepare(
            'SELECT * FROM Candidacy WHERE Joboffer_id IN ('.implode(',', $jobIds).')'
        );
        $manager->execute();

        $candidacies = $manager->fetchAll();
        $candidaciesByJobId = [];

        foreach($candidacies as $candidacy) {
            $candidaciesByJobId[$candidacy["Joboffer_id"]] = true;
        }

        return $this->render('joboffer/index.html.twig', compact('joboffer', 'candidaciesByJobId'));

        // $query = 'SELECT * FROM Joboffer';

        // $manager = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        // $manager->execute();

        // $joboffer = $manager->fetchAll();
        

        // $joboffers = $this->getDoctrine()
        //     ->getRepository(Joboffer::class)
        //     ->findAll();

        return $this->render('joboffer/index.html.twig', compact('joboffer'));
    }
    /**
     * @Route("/new", name="joboffer_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $joboffer = new Joboffer();
        $form = $this->createForm(JobofferType::class, $joboffer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($joboffer);
            $em->flush();

            return $this->redirectToRoute('joboffer_index');
        }

        return $this->render('joboffer/new.html.twig', [
            'joboffer' => $joboffer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/joboffer/show/{id}", name="show")
     */
    public function show(Joboffer $joboffer): Response
    {
        $candidate = $this->getUser();
        $allApplication = $joboffer->getCandidate()->toArray();

        $alreadyApply = false;

        foreach ($allApplication as $candidateApply) {
            // dd($candidateApply->getId() === $candidate->getId());
            if ($candidateApply->getId() === $candidate->getId()) {
                $alreadyApply = true;
            } else {
                $alreadyApply = false;
            }
        }
        // dd($alreadyApply);
        // $query = 'SELECT * FROM Joboffer
        // WHERE id = "'.$joboffer->getId().'"';

        // $manager = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        // $manager->execute();

        // $job = $manager->fetchAll();
        
        return $this->render('joboffer/show.html.twig', compact('joboffer', 'alreadyApply'));
    }
  



    /**
     * @Route("/{id}/edit", name="joboffer_edit", methods="GET|POST")
     */
    public function edit(Request $request, Joboffer $joboffer): Response
    {
        $form = $this->createForm(JobofferType::class, $joboffer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('joboffer_edit', ['id' => $joboffer->getId()]);
        }

        return $this->render('joboffer/edit.html.twig', [
            'joboffer' => $joboffer,
            'form' => $form->createView(),
        ]);
    }

    // /**
    //  * @Route("/{id}", name="joboffer_delete", methods="DELETE")
    //  */
    // public function delete(Request $request, Joboffer $joboffer): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$joboffer->getId(), $request->request->get('_token'))) {
    //         $em = $this->getDoctrine()->getManager();
    //         $em->remove($joboffer);
    //         $em->flush();
    //     }

    //     return $this->redirectToRoute('joboffer_index');
    // }


}
