<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

     /**
     * @Route("/company", name="about")
     */
    public function about()
    {
        return $this->render('compagny/company.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

     /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        return $this->render('home/contact.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    //  /**
    //  * @Route("/login", name="login")
    //  */
    // public function login()
    // {
    //     return $this->render('security/login.html.twig', [
    //         'controller_name' => 'HomeController',
    //     ]);
    // }
}
