<?php

namespace App\Controller;

use App\Entity\Candidate;
use App\Form\CandidateType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CandidateController extends AbstractController
{

    /**
     * Affichage du profil
     * 
     * @Route("/profile", name="profile")
     */
    public function profile() 
    {
        $candidate = $this->getUser();

        $shouldBeFilled = [
            'gender', 'firstname', 'lastname','adress','country','nationality','passport', 'cv','picture','currentlocation','dateofbirth','placeofbirth','email','availability','jobsector','experience','shortdescription',
        ];
        $filledFields = 0;
        $isNotFilled = false;

        foreach ($shouldBeFilled as $field){
            if(!$field) {
                $isNotFilled = true;
                $filledFields++;
            }
        
            //  ('Votre profil est rempli a 100%' . (int)($filledFields / count($shouldBeFilled) * 100) . '%');
        }

         return $this->render('security/profile.html.twig', [
         'controller_name' => 'SecurityController',
         'candidate' => $candidate,
         'isNotFilled' => $isNotFilled,
         'completionPercentage' => ($filledFields / count($shouldBeFilled)) * 100
        ]);
    }

    /**
     * Mise à jour du profil après envoi du formulaire
     * 
     * @Route("/edit", name="candidate_edit", methods="GET|POST")
     */
    public function edit(Request $request): Response
    {
        $candidate = $this->getUser();

        $Dateofbirth = new \DateTime($request->request->get('birth_date'));

        $candidate->setFirstname($request->request->get('first_name'));
        $candidate->setLastname($request->request->get('last_name'));
        $candidate->setGender($request->request->get('gender'));
        $candidate->setCurrentlocation($request->request->get('current_location'));
        $candidate->setAdress($request->request->get('address'));
        $candidate->setCountry($request->request->get('country'));
        $candidate->setNationality($request->request->get('nationality'));
        $candidate->setDateofbirth($Dateofbirth);
        $candidate->setPlaceofbirth($request->request->get('birth_place'));
        $candidate->setPicture($request->request->get('picture'));
        $candidate->setPassport($request->request->get('passport'));
        $candidate->setCv($request->request->get('cv'));
        $candidate->setAvailability($request->request->get('availability'));
        $candidate->setJobsector($request->request->get('job_sector'));
        $candidate->setExperience($request->request->get('experience'));
        $candidate->setShortdescription($request->request->get('description'));


        $em = $this->getDoctrine()->getManager();
        $em->persist($candidate);
        $em->flush();


        $shouldBeFilled = [
            'gender', 'firstname', 'lastname', 'currentlocation', 'adress', 'country', 'nationality', 'dateofbirth', 'placeofbirth', 'jobsector', 'experience', 'shortdescription', 'cv', 'picture', 'passport'
        ];
        $filledFields = 0;
        $isNotFilled = false;
        

        foreach ($shouldBeFilled as $field){
            // Exemple : getGender, getFirstName
            $property = "get".ucfirst($field);
            
            if (!$candidate->$property()){
                $isNotFilled = true;
            }
            else {
                $filledFields++; 
            }
            // ('Votre profil est rempli à 100% . (int)($filledFields / count($shouldBeFilled) * 100) .'%');
        }


        return $this->render('security/profile.html.twig', [
            'controller_name' => 'SecurityController',
            'candidate' => $candidate,
            'isNotFilled' => $isNotFilled,
            'completionPercentage' => ($filledFields / count($shouldBeFilled)) * 100
           ]);


        
        // $request = $request->get('gender', 'firstname', 'lastname','adress','country','nationality','currentlocation','dateofbirth','placeofbirth','email','availability','jobsector','experience','shortdescription');
        
        

        
        // $form = $this->createForm(CandidateType::class, $candidate);
        // $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {

            
            // $request->files->get('candidate');
            // dd($request->files->get('candidate'));

        //     $this->getDoctrine()->getManager()->flush();

        //     return $this->redirectToRoute('candidate_edit', ['id' => $candidate->getId()]);
        // }

        return $this->render('candidate/edit.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/candidate/{id}", name="candidate_delete", methods="DELETE")
     */
    public function delete(Request $request, Candidate $candidate): Response
    {
        if ($this->isCsrfTokenValid('delete'.$candidate->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($candidate);
            $em->flush();
        }

        return $this->redirectToRoute('candidate_index');
    }
    
    /**
     * @Route("/profile", name="candidate_index", methods="GET")
     */
    public function index(): Response
    {
        $candidates = $this->getDoctrine()
            ->getRepository(Candidate::class)
            ->findAll();

        return $this->render('security/profile.html.twig', ['candidates' => $candidates]);
    }

    /**
     * @Route("/new", name="candidate_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $candidate = new Candidate();
        $form = $this->createForm(CandidateType::class, $candidate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($candidate);
            $em->flush();

            return $this->redirectToRoute('candidate_index');
        }

        return $this->render('candidate/new.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }
}
