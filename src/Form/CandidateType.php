<?php

namespace App\Form;

use App\Entity\Candidate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CandidateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender')
            ->add('firstname')
            ->add('lastname')
            ->add('adress')
            ->add('country')
            ->add('nationality')
            ->add('passport')
            ->add('cv')
            ->add('picture')
            ->add('currentlocation')
            ->add('dateofbirth')
            ->add('placeofbirth')
            ->add('email')
            ->add('password')
            ->add('availability')
            ->add('jobsector')
            ->add('experience')
            ->add('shortdescription')
            ->add('admin')
            ->add('createdat')
            ->add('updatedat')
            ->add('notes')
            ->add('deletedat')
            ->add('joboffer')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Candidate::class,
        ]);
    }
}
