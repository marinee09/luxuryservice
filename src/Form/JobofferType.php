<?php

namespace App\Form;

use App\Entity\Joboffer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobofferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference')
            ->add('description')
            ->add('valide')
            ->add('notes')
            ->add('jobtitle')
            ->add('jobtype')
            ->add('location')
            ->add('jobcategory')
            ->add('closingdate')
            ->add('salary')
            ->add('createdat')
            ->add('updatedat')
            ->add('customer')
            ->add('candidate')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Joboffer::class,
        ]);
    }
}
